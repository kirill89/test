'use strict';

const _ = require('underscore');
const RedditFormatter = require('./');
const util = require('util');
const EOL = require('os').EOL;

/**
 * @param {string} tableName - Table name to insert values.
 * @constructor
 */
function SQLRedditFormatter(tableName) {
	this.tableName = tableName;
}
util.inherits(SQLRedditFormatter, RedditFormatter);

SQLRedditFormatter.prototype.format = function(data) {
	if (!data.length) {
		return '';
	}

	/*
	 * `JSON.stringify` used to escape double quotes and correctly wrap strings.
	 */
	let result = '';
	let fields = _.keys(data[0]);
	let fieldsString = fields.join(',');

	data.forEach(row => {
		result += `INSERT INTO ${this.tableName} (${fieldsString}) VALUES (`;

		result += fields
			.map(field => JSON.stringify(row[field]))
			.join(',');

		result += `)${EOL}`;
	});

	return result;
};

module.exports = SQLRedditFormatter;
