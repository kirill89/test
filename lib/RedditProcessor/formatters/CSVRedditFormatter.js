'use strict';

const _ = require('underscore');
const RedditFormatter = require('./');
const util = require('util');
const EOL = require('os').EOL;

/**
 * @param {string} delimiter - column delimiter "," or ";", default ","
 * @constructor
 */
function CSVRedditFormatter(delimiter) {
	this.delimiter = delimiter === ';' ? delimiter : ',';
}
util.inherits(CSVRedditFormatter, RedditFormatter);

CSVRedditFormatter.prototype.format = function(data) {
	if (!data.length) {
		return '';
	}

	/*
	 * `JSON.stringify` used to escape double quotes and correctly wrap strings.
	 */

	/*
	 * Fill headers.
	 */
	let fields = _.keys(data[0]);
	let result = fields
		.map(f => JSON.stringify(f))
		.join(this.delimiter);

	result += EOL;

	/*
	 * Fill data.
	 */
	data.forEach(row => {
		result += fields
			.map(field => JSON.stringify(row[field]))
			.join(this.delimiter);

		result += EOL;
	});

	return result;
};

module.exports = CSVRedditFormatter;
