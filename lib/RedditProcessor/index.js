'use strict';

const _ = require('underscore');
const conform = require('conform');
const requestPromise = require('request-promise');

/**
 * @param {RedditHandler} handler concrete RedditHandler implementation.
 * @param {RedditFormatter} formatter concrete RedditFormatter implementation.
 * @class
 */
function RedditProcessor(handler, formatter) {
	this.handler = handler;
	this.formatter = formatter;
}

/**
 * @param {object} data - Parsed JSON.
 * @return {object} Validation result (valid, errors).
 * @private
 */
RedditProcessor.prototype._validate = function(data) {
	let ensureFields = {};

	_.each(this.handler.fields(), (type, name) => {
		ensureFields[name] = {type, required: true};
	}, {});

	return conform.validate(data, {
		properties: {
			data: {
				type: 'object',
				required: true,
				properties: {
					children: {
						type: 'array',
						required: true,
						items: {
							type: 'object',
							properties: {
								data: {
									type: 'object',
									required: true,
									properties: ensureFields
								}
							}
						}
					}
				}
			}
		}
	});
};

/**
 * @param {string} url - URL address to download JSON file.
 * @return {Promise<object>} JSON data from reddit.
 * @private
 */
RedditProcessor.prototype._load = function(url) {
	return requestPromise({
		uri: url,
		json: true
	});
};

/**
 * @param {string} url - URL address to download JSON file.
 * @return {Promise<string>} Processed and formatted data from reddit.
 */
RedditProcessor.prototype.process = function(url) {
	return this
		._load(url)
		.then(responseData => {
			let validationResult = this._validate(responseData);

			if (!validationResult.valid) {
				let desc = validationResult.errors
					.map(e => `${e.property} ${e.message}`)
					.join('; ');

				throw new Error(`RedditProcessor: validation error - ${desc}`);
			}

			return responseData.data.children;
		})
		.then(data => this.handler.handle(data))
		.then(data => this.formatter.format(data));
};

module.exports = RedditProcessor;
