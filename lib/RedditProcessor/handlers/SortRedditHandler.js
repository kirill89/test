'use strict';

const _ = require('underscore');
const RedditHandler = require('./');
const util = require('util');

/**
 * @param {string} sortField field for sorting, default "id"
 * @param {string} sortOrder - ASC or DESC, default ASC
 * @constructor
 */
function SortRedditHandler(sortField, sortOrder) {
	this.sortOrder = sortOrder === 'DESC' ? sortOrder : 'ASC';

	let availableFields = _.keys(this.fields());

	if (availableFields.indexOf(sortField) === -1) {
		this.sortField = 'id';
	} else {
		this.sortField = sortField;
	}
}
util.inherits(SortRedditHandler, RedditHandler);

SortRedditHandler.prototype.fields = function() {
	/* eslint-disable camelcase */
	return {
		id: 'string',
		title: 'string',
		created_utc: 'number',
		score: 'number'
	};
};

/**
 * @param {array} data - Array of items with reddit fields.
 * @return {array} Sorted data with restricted fields.
 */
SortRedditHandler.prototype.handle = function(data) {
	let fields = _.keys(this.fields());
	let cleanData = data.map(item => _.pick(item.data, fields));
	let sortedData = _.sortBy(cleanData, this.sortField);

	return this.sortOrder === 'ASC' ? sortedData : sortedData.reverse();
};

module.exports = SortRedditHandler;
