'use strict';

const _ = require('underscore');
const RedditHandler = require('./');
const util = require('util');

/**
 * @constructor
 */
function AggregateRedditHandler() {
}
util.inherits(AggregateRedditHandler, RedditHandler);

AggregateRedditHandler.prototype.fields = function() {
	return {
		domain: 'string',
		score: 'number'
	};
};

/**
 * @param {array} data - Array of items with reddit fields.
 * @return {array} Aggregated data for each domain: domain, articles count and
 * total score.
 */
AggregateRedditHandler.prototype.handle = function(data) {
	let aggregated = data.reduce((acc, item) => {
		let domain = item.data.domain;
		let domainData = acc[domain] = acc[domain] || {articles: 0, score: 0};

		domainData.articles++;
		domainData.score += item.data.score;

		return acc;
	}, {});

	return _.keys(aggregated).map(domain => ({
		domain,
		score: aggregated[domain].score,
		count: aggregated[domain].articles
	}));
};

module.exports = AggregateRedditHandler;
