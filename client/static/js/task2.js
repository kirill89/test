'use strict';

/* global $ */

/*
 * 1. Will not work in old browsers, but in this project no matter.
 * 2. Maybe better use backbone or angular here, but it can be redundant.
 */

$(() => {
	const DEMO_DATA = [
		{id: 1, parentId: 0},
		{id: 2, parentId: 0},
		{id: 3, parentId: 1},
		{id: 4, parentId: 1},
		{id: 5, parentId: 2},
		{id: 6, parentId: 4},
		{id: 7, parentId: 5}
	];

	const $in = $('.task2-in');
	const $out = $('.task2-out');
	const $err = $('.task2-error');

	$in.val(JSON.stringify(DEMO_DATA, null, 2));

	$('.task2-run').click(function() {
		$err.addClass('hidden');

		let data;

		try {
			data = JSON.parse($in.val());
		} catch (e) {
			$err.text('Can\'t parse JSON').removeClass('hidden');
			return false;
		}

		let result = [];
		/*
		 * id -> node
		 */
		let index = {};

		data.forEach(node => {
			index[node.id] = node;

			if (node.parentId) {
				index[node.parentId].children = index[node.parentId].children || [];
				index[node.parentId].children.push(node);
			} else {
				result.push(node);
			}
		});

		$out.val(JSON.stringify(result, null, 2));

		return false;
	});
});
