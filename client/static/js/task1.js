'use strict';

/* global $ */

/*
 * 1. Will not work in old browsers, but in this project no matter.
 * 2. Maybe better use backbone or angular here, but it can be redundant.
 */

$(() => {
	/*
	 * Show selected handler options.
	 */
	$('[name="handler"]').change(function() {
		let handler = $(this).val();
		let $sort = $('.handler-sort');

		if (handler === 'sort') {
			$sort.removeClass('hidden');
		} else {
			$sort.addClass('hidden');
		}
	});

	/*
	 * Show selected formatter options.
	 */
	let formattersControllGroups = {
		csv: $('.formatter-csv'),
		sql: $('.formatter-sql')
	};

	$('[name="formatter"]').change(function() {
		let formatter = $(this).val();

		Object.keys(formattersControllGroups).forEach(key => {
			if (key === formatter) {
				formattersControllGroups[key].removeClass('hidden');
			} else {
				formattersControllGroups[key].addClass('hidden');
			}
		});
	});

	/*
	 * Process "Parse" button click.
	 */
	$('.main-form').submit(function() {
		let $spinner = $('.loading');
		let $error = $('.error');
		let $result = $('.result');

		$error.text('').addClass('hidden');
		$spinner.removeClass('hidden');

		$.post('/', $(this).serialize())
			.done(res => {
				$result.text(res);
			})
			.fail(err => {
				$error.text(err.responseText).removeClass('hidden');
			})
			.always(() => {
				$spinner.addClass('hidden');
			});

		return false;
	});
});
