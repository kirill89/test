'use strict';

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const RedditProcessor = require('./lib/RedditProcessor');
const CSVRedditFormatter =
	require('./lib/RedditProcessor/formatters/CSVRedditFormatter');
const SQLRedditFormatter =
	require('./lib/RedditProcessor/formatters/SQLRedditFormatter');
const AggregateRedditHandler =
	require('./lib/RedditProcessor/handlers/AggregateRedditHandler');
const SortRedditHandler =
	require('./lib/RedditProcessor/handlers/SortRedditHandler');

const LISTEN_PORT = 3000;

express()
	.use('/', express.static(path.join(__dirname, 'client', 'static')))
	.use(bodyParser.urlencoded({extended: true}))
	.set('view engine', 'jade')
	.set('views', path.join(__dirname, 'client', 'templates'))

	/*
	 * Show parser options.
	 */
	.get('/', (req, res) => {
		res.render('index');
	})

	/*
	 * Parse.
	 */
	.post('/', (req, res) => {
		let formatter;
		let handler;

		switch (req.body.formatter) {
		case 'csv':
			formatter = new CSVRedditFormatter(req.body.delimiter);
			break;
		default:
			formatter = new SQLRedditFormatter(req.body.tableName);
		}

		switch (req.body.handler) {
		case 'aggregate':
			handler = new AggregateRedditHandler();
			break;
		default:
			handler =
				new SortRedditHandler(req.body.sortField, req.body.sortOrder);
		}

		let processor = new RedditProcessor(handler, formatter);

		processor
			.process(req.body.url)
			.then(text => {
				res.send(text);
			})
			.catch(err => {
				res.status(500);
				res.send(String(err));
			});
	})

	.listen(LISTEN_PORT, () => {
		console.log(`Started on ${LISTEN_PORT}...`);
	});
